﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using System.Collections;

namespace ImageCompressedUI
{
    public partial class ImgCompressed : Form
    {
        // folder with compressed img
        string folderCompressedImg = String.Format("{0}\\SaveImg", Environment.CurrentDirectory);
        static int fileInList = 0;
        ArrayList buttons;

        public ImgCompressed()
        {
            InitializeComponent();
            this.MaximizeBox = false;

            buttons = new ArrayList() {button_addfile, button_removefile, button_openfolder, button_clear,
                                          butt_openFolder, button_start, trackBarOption };

            if (!Directory.Exists(folderCompressedImg))
                Directory.CreateDirectory(folderCompressedImg);
        }

        // disabling buttons during conversion
        void ActiveButton()
        {
            foreach(object item in buttons)
            {
                Button b = item is Button ? (Button)item: new Button();
                TrackBar t = item is TrackBar ? (TrackBar)item : new TrackBar();
                b.Enabled = true;
                t.Enabled = true;
            }
        }

        void DeactiveButton()
        {
            foreach (object item in buttons)
            {
                Button b = item is Button ? (Button)item : new Button();
                TrackBar t = item is TrackBar ? (TrackBar)item : new TrackBar();
                b.Enabled = false;
                t.Enabled = false;
            }
        }

        private async void button_start_Click(object sender, EventArgs e)
        {
            if (listViewWin.Items.Count == 0)
            {
                MessageBox.Show("Список для обработки пуст. Добавьте файлы для обработки.");
                return;
            }

            // Image compression option. TraskBar value (0-10) * 250
            int option = trackBarOption.Value * 250;
            int count = 1;

            // Disable button in time process
            DeactiveButton();

            foreach (ListViewItem file in listViewWin.Items)
            {
                try
                {
                    FileInfo fileInfo = new FileInfo(file.Text);
                    string newfile = folderCompressedImg + "\\compressed_" + fileInfo.Name;

                    Image image = Image.FromFile(file.Text);
                    await Task.Run(() => { ResizeOrigImg(image, option, option, newfile); });

                    toolStripStatusLabelSL.Text = String.Format("{0}/{1}", count++, listViewWin.Items.Count);
                    
                    //garbage collection
                    image.Dispose();
                }
                catch (Exception)
                {
                    continue;
                }
            }

            // Enabled button in time process
            ActiveButton();
            MessageBox.Show("Complete!");
        }

        private void button_addfile_Click(object sender, EventArgs e)
        {
            if (openFileDialogAdd.ShowDialog() == DialogResult.OK)
            {
                // check file for duplicates in list
                ListViewItem itemIsListed = listViewWin.FindItemWithText(openFileDialogAdd.FileName);
                if (itemIsListed == null)
                {
                    listViewWin.Items.Add(openFileDialogAdd.FileName);
                    labelCountFile.Text = $"{fileInList += 1}";
                }
            }
        }

        private void button_removefile_Click(object sender, EventArgs e)
        {
            listViewWin.Items.Remove(listViewWin.FocusedItem);
            labelCountFile.Text = $"{fileInList -= 1}";
        }

        private void button_clear_Click(object sender, EventArgs e)
        {
            listViewWin.Items.Clear();
            labelCountFile.Text = $"{fileInList = 0}";
        }

        private void button_addfolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialogAdd.ShowDialog() == DialogResult.OK)
            {
                // Get a list of files from the specified directory
                string path = folderBrowserDialogAdd.SelectedPath;
                string[] files = Directory.GetFiles(path).Where(c => c.EndsWith(".bmp") || c.EndsWith(".png") || c.EndsWith(".jpg") || c.EndsWith(".jpeg")).ToArray();

                foreach (var item in files)
                {
                    // check file for duplicates in list
                    ListViewItem itemIsListed = listViewWin.FindItemWithText(item);
                    if (itemIsListed != null) continue;

                    listViewWin.Items.Add(item);
                    labelCountFile.Text = $"{fileInList += 1}";
                }
            }
        }

        private void button_openFolder_Click(object sender, EventArgs e)
        {
            Process.Start(folderCompressedImg);
        }

        private void listViewWin_DoubleClick(object sender, EventArgs e)
        {
            Process.Start(listViewWin.FocusedItem.Text);
        }

        // ################ Custom methods ######################
        // Resize image
        public static void ResizeOrigImg(Image image, int nWidth, int nHeight, string newPath)
        {
            int newWidth, newHeight;
            var coefH = (double)nHeight / (double)image.Height;
            var coefW = (double)nWidth / (double)image.Width;
            if (coefW >= coefH)
            {
                newHeight = (int)(image.Height * coefH);
                newWidth = (int)(image.Width * coefH);
            }
            else
            {
                newHeight = (int)(image.Height * coefW);
                newWidth = (int)(image.Width * coefW);
            }

            Image result = new Bitmap(newWidth, newHeight);
            using (var g = Graphics.FromImage(result))
            {
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                g.DrawImage(image, 0, 0, newWidth, newHeight);
                g.Dispose();
            }
            result.Save(newPath);
        }

        private void checkBoxTop_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            ImgCompressed.ActiveForm.TopMost = checkBox.Checked ? true : false;
        }

        void listViewWin_DragDrop(object sender, DragEventArgs e)
        {
            List<string> paths = new List<string>();

            // Get a list of files from DnD
            foreach (string obj in (string[])e.Data.GetData(DataFormats.FileDrop))
            {
                if (Directory.Exists(obj))
                    paths.AddRange(Directory.GetFiles(obj, "*.*", SearchOption.AllDirectories));
                else
                    paths.Add(obj);
            }

            // Filtred files on extension image
            foreach (string file in paths.Where(c => c.EndsWith(".bmp") || c.EndsWith(".png") || c.EndsWith(".jpg") || c.EndsWith(".jpeg")))
            {
                // check file for duplicates in list
                ListViewItem itemIsListed = listViewWin.FindItemWithText(file);
                if (itemIsListed != null) continue;

                listViewWin.Items.Add(file);
                fileInList ++;
            }
            labelCountFile.Text = $"{fileInList}";
        }

        void listViewWin_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
        }
    }
}
