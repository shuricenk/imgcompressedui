﻿namespace ImageCompressedUI
{
    partial class ImgCompressed
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImgCompressed));
            this.listViewWin = new System.Windows.Forms.ListView();
            this.imageListIco = new System.Windows.Forms.ImageList(this.components);
            this.button_start = new System.Windows.Forms.Button();
            this.butt_openFolder = new System.Windows.Forms.Button();
            this.button_addfile = new System.Windows.Forms.Button();
            this.button_removefile = new System.Windows.Forms.Button();
            this.openFileDialogAdd = new System.Windows.Forms.OpenFileDialog();
            this.toolTipOpenInfo = new System.Windows.Forms.ToolTip(this.components);
            this.button_clear = new System.Windows.Forms.Button();
            this.button_openfolder = new System.Windows.Forms.Button();
            this.trackBarOption = new System.Windows.Forms.TrackBar();
            this.checkBoxTop = new System.Windows.Forms.CheckBox();
            this.folderBrowserDialogAdd = new System.Windows.Forms.FolderBrowserDialog();
            this.statusStripProcess = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelST = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelSE = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelSL = new System.Windows.Forms.ToolStripStatusLabel();
            this.labelInfoAddFiles = new System.Windows.Forms.Label();
            this.labelCountFile = new System.Windows.Forms.Label();
            this.labelMin = new System.Windows.Forms.Label();
            this.labelMax = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarOption)).BeginInit();
            this.statusStripProcess.SuspendLayout();
            this.SuspendLayout();
            // 
            // listViewWin
            // 
            this.listViewWin.AllowDrop = true;
            this.listViewWin.HideSelection = false;
            this.listViewWin.Location = new System.Drawing.Point(12, 24);
            this.listViewWin.Name = "listViewWin";
            this.listViewWin.Size = new System.Drawing.Size(416, 209);
            this.listViewWin.SmallImageList = this.imageListIco;
            this.listViewWin.TabIndex = 0;
            this.toolTipOpenInfo.SetToolTip(this.listViewWin, "Список файлов для обработки. Двойной клик по файлу открывает его на просмотр.");
            this.listViewWin.UseCompatibleStateImageBehavior = false;
            this.listViewWin.View = System.Windows.Forms.View.List;
            this.listViewWin.DragDrop += new System.Windows.Forms.DragEventHandler(this.listViewWin_DragDrop);
            this.listViewWin.DragEnter += new System.Windows.Forms.DragEventHandler(this.listViewWin_DragEnter);
            this.listViewWin.DoubleClick += new System.EventHandler(this.listViewWin_DoubleClick);
            // 
            // imageListIco
            // 
            this.imageListIco.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIco.ImageStream")));
            this.imageListIco.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIco.Images.SetKeyName(0, "iconfinder_OK_132710.png");
            // 
            // button_start
            // 
            this.button_start.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_start.Location = new System.Drawing.Point(329, 268);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(99, 39);
            this.button_start.TabIndex = 1;
            this.button_start.Text = "Начать";
            this.toolTipOpenInfo.SetToolTip(this.button_start, "Начать обработку списка файлов.");
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // butt_openFolder
            // 
            this.butt_openFolder.Location = new System.Drawing.Point(329, 239);
            this.butt_openFolder.Name = "butt_openFolder";
            this.butt_openFolder.Size = new System.Drawing.Size(99, 23);
            this.butt_openFolder.TabIndex = 4;
            this.butt_openFolder.Text = "Открыть";
            this.toolTipOpenInfo.SetToolTip(this.butt_openFolder, "Открыть папку с обработанными файлами.");
            this.butt_openFolder.UseVisualStyleBackColor = true;
            this.butt_openFolder.Click += new System.EventHandler(this.button_openFolder_Click);
            // 
            // button_addfile
            // 
            this.button_addfile.Location = new System.Drawing.Point(12, 239);
            this.button_addfile.Name = "button_addfile";
            this.button_addfile.Size = new System.Drawing.Size(99, 23);
            this.button_addfile.TabIndex = 4;
            this.button_addfile.Text = "Добавить";
            this.toolTipOpenInfo.SetToolTip(this.button_addfile, "Добавить файл в список для обработки.");
            this.button_addfile.UseVisualStyleBackColor = true;
            this.button_addfile.Click += new System.EventHandler(this.button_addfile_Click);
            // 
            // button_removefile
            // 
            this.button_removefile.Location = new System.Drawing.Point(117, 239);
            this.button_removefile.Name = "button_removefile";
            this.button_removefile.Size = new System.Drawing.Size(99, 23);
            this.button_removefile.TabIndex = 4;
            this.button_removefile.Text = "Удалить";
            this.toolTipOpenInfo.SetToolTip(this.button_removefile, "Удалить элемент из списка.");
            this.button_removefile.UseVisualStyleBackColor = true;
            this.button_removefile.Click += new System.EventHandler(this.button_removefile_Click);
            // 
            // openFileDialogAdd
            // 
            this.openFileDialogAdd.Filter = "Images|*.bmp;*.png;*.jpg;*.jpeg";
            // 
            // button_clear
            // 
            this.button_clear.Location = new System.Drawing.Point(117, 268);
            this.button_clear.Name = "button_clear";
            this.button_clear.Size = new System.Drawing.Size(99, 39);
            this.button_clear.TabIndex = 6;
            this.button_clear.Text = "Очистить список";
            this.toolTipOpenInfo.SetToolTip(this.button_clear, "Очистить список в главном окне.");
            this.button_clear.UseVisualStyleBackColor = true;
            this.button_clear.Click += new System.EventHandler(this.button_clear_Click);
            // 
            // button_openfolder
            // 
            this.button_openfolder.Location = new System.Drawing.Point(12, 268);
            this.button_openfolder.Name = "button_openfolder";
            this.button_openfolder.Size = new System.Drawing.Size(99, 39);
            this.button_openfolder.TabIndex = 6;
            this.button_openfolder.Text = "Добавить папку";
            this.toolTipOpenInfo.SetToolTip(this.button_openfolder, "Выбрать целиком папку для обработки.");
            this.button_openfolder.UseVisualStyleBackColor = true;
            this.button_openfolder.Click += new System.EventHandler(this.button_addfolder_Click);
            // 
            // trackBarOption
            // 
            this.trackBarOption.Location = new System.Drawing.Point(219, 239);
            this.trackBarOption.Name = "trackBarOption";
            this.trackBarOption.Size = new System.Drawing.Size(104, 45);
            this.trackBarOption.TabIndex = 9;
            this.toolTipOpenInfo.SetToolTip(this.trackBarOption, "Выбрать режим сжатия изображений.");
            this.trackBarOption.Value = 5;
            // 
            // checkBoxTop
            // 
            this.checkBoxTop.AutoSize = true;
            this.checkBoxTop.Location = new System.Drawing.Point(332, 4);
            this.checkBoxTop.Name = "checkBoxTop";
            this.checkBoxTop.Size = new System.Drawing.Size(96, 17);
            this.checkBoxTop.TabIndex = 10;
            this.checkBoxTop.Text = "Поверх окон";
            this.toolTipOpenInfo.SetToolTip(this.checkBoxTop, "Закрепить программу поверх окон.");
            this.checkBoxTop.UseVisualStyleBackColor = true;
            this.checkBoxTop.CheckedChanged += new System.EventHandler(this.checkBoxTop_CheckedChanged);
            // 
            // statusStripProcess
            // 
            this.statusStripProcess.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelST,
            this.toolStripStatusLabelSE,
            this.toolStripStatusLabelSL});
            this.statusStripProcess.Location = new System.Drawing.Point(0, 318);
            this.statusStripProcess.Name = "statusStripProcess";
            this.statusStripProcess.Size = new System.Drawing.Size(440, 22);
            this.statusStripProcess.TabIndex = 8;
            this.statusStripProcess.Text = "statusStrip1";
            // 
            // toolStripStatusLabelST
            // 
            this.toolStripStatusLabelST.Name = "toolStripStatusLabelST";
            this.toolStripStatusLabelST.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabelSE
            // 
            this.toolStripStatusLabelSE.Name = "toolStripStatusLabelSE";
            this.toolStripStatusLabelSE.Size = new System.Drawing.Size(81, 17);
            this.toolStripStatusLabelSE.Text = "Обработано: ";
            // 
            // toolStripStatusLabelSL
            // 
            this.toolStripStatusLabelSL.Name = "toolStripStatusLabelSL";
            this.toolStripStatusLabelSL.Size = new System.Drawing.Size(26, 17);
            this.toolStripStatusLabelSL.Text = "0/0";
            // 
            // labelInfoAddFiles
            // 
            this.labelInfoAddFiles.AutoSize = true;
            this.labelInfoAddFiles.Location = new System.Drawing.Point(12, 8);
            this.labelInfoAddFiles.Name = "labelInfoAddFiles";
            this.labelInfoAddFiles.Size = new System.Drawing.Size(113, 13);
            this.labelInfoAddFiles.TabIndex = 5;
            this.labelInfoAddFiles.Text = "Добавлено файлов:";
            // 
            // labelCountFile
            // 
            this.labelCountFile.AutoSize = true;
            this.labelCountFile.Location = new System.Drawing.Point(120, 8);
            this.labelCountFile.Name = "labelCountFile";
            this.labelCountFile.Size = new System.Drawing.Size(13, 13);
            this.labelCountFile.TabIndex = 5;
            this.labelCountFile.Text = "0";
            // 
            // labelMin
            // 
            this.labelMin.AutoSize = true;
            this.labelMin.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMin.Location = new System.Drawing.Point(222, 263);
            this.labelMin.Name = "labelMin";
            this.labelMin.Size = new System.Drawing.Size(16, 21);
            this.labelMin.TabIndex = 11;
            this.labelMin.Text = "-";
            // 
            // labelMax
            // 
            this.labelMax.AutoSize = true;
            this.labelMax.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMax.Location = new System.Drawing.Point(302, 263);
            this.labelMax.Name = "labelMax";
            this.labelMax.Size = new System.Drawing.Size(21, 21);
            this.labelMax.TabIndex = 11;
            this.labelMax.Text = "+";
            // 
            // ImgCompressed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 340);
            this.Controls.Add(this.labelMax);
            this.Controls.Add(this.labelMin);
            this.Controls.Add(this.checkBoxTop);
            this.Controls.Add(this.trackBarOption);
            this.Controls.Add(this.statusStripProcess);
            this.Controls.Add(this.button_openfolder);
            this.Controls.Add(this.button_clear);
            this.Controls.Add(this.labelCountFile);
            this.Controls.Add(this.labelInfoAddFiles);
            this.Controls.Add(this.button_removefile);
            this.Controls.Add(this.button_addfile);
            this.Controls.Add(this.butt_openFolder);
            this.Controls.Add(this.button_start);
            this.Controls.Add(this.listViewWin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ImgCompressed";
            this.Text = "Compressed Image";
            ((System.ComponentModel.ISupportInitialize)(this.trackBarOption)).EndInit();
            this.statusStripProcess.ResumeLayout(false);
            this.statusStripProcess.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewWin;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Button butt_openFolder;
        private System.Windows.Forms.Button button_addfile;
        private System.Windows.Forms.Button button_removefile;
        private System.Windows.Forms.OpenFileDialog openFileDialogAdd;
        private System.Windows.Forms.ToolTip toolTipOpenInfo;
        private System.Windows.Forms.Button button_clear;
        private System.Windows.Forms.Button button_openfolder;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogAdd;
        private System.Windows.Forms.StatusStrip statusStripProcess;
        private System.Windows.Forms.ImageList imageListIco;
        private System.Windows.Forms.TrackBar trackBarOption;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelST;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelSE;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelSL;
        private System.Windows.Forms.Label labelInfoAddFiles;
        private System.Windows.Forms.Label labelCountFile;
        private System.Windows.Forms.CheckBox checkBoxTop;
        private System.Windows.Forms.Label labelMin;
        private System.Windows.Forms.Label labelMax;
    }
}

